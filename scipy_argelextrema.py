from pylab import *
from scipy.io import wavfile
from scipy.signal import argrelextrema, find_peaks_cwt

# sampFreq, snd = wavfile.read('audio.wav')
sampFreq, snd = wavfile.read('beep.wav')

print(snd.dtype)

snd = snd / (2. ** 15)

print(snd.shape)
print(snd)

if len(snd.shape) == 1:
    s1 = snd
else:
    s1 = snd[:, 0]

print(s1.dtype)
print(s1)

timeArray = arange(0, snd.shape[0], 1)
timeArray = timeArray / sampFreq
timeArray = timeArray * 1000

# indexes = find_peaks_cwt(s1, np.arange(1, 4), max_distances=np.arange(1, 4) * 2)
# indexes = np.array(indexes) - 1

# indexes = argrelextrema(np.array(s1), comparator=np.greater, order=2)

highFreq = np.amax(s1)
index = 0
for i in range(0, len(s1)):
    if s1[i] == highFreq:
        index = i

print(index)
print(timeArray[index] / 1000)

plot(timeArray, s1, color='k')
ylabel('Amplitude')
xlabel('Time (ms)')

x = 1
