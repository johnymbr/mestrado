import matplotlib.pyplot as plt
import sys
import wave
from sklearn.preprocessing import normalize, MinMaxScaler, minmax_scale
import numpy as np
from subprocess import call
import math

deviation_size_default = 1000


# extract audio from video file, save as wav audio file
# INPUT: video file
# OUTPUT: Does not return any values,  but saves audio as wav file
def extract_audio(dir, video_file):
    track_name = video_file.split(".")
    audio_output = track_name[0] + "WAV.wav"  # !! CHECK TO SEE IF FILE IS IN UPLOADS DIRECTORY
    output = dir + audio_output
    call(["avconv", "-y", "-i", dir + video_file, "-vn", "-ac", "1", "-f", "wav", output])
    return output


def normalize_list(list_normal):
    max_value = np.max(list_normal)
    min_value = np.min(list_normal)
    for i in range(len(list_normal)):
        list_normal[i] = (list_normal[i] - min_value) / (max_value - min_value)

    return list_normal


def normalize_list_numpy(list_numpy):
    normalized_list = minmax_scale(list_numpy)
    return normalized_list


def somar(array_numpy):
    soma = 0
    for el in range(len(array_numpy)):
        soma += array_numpy[el]

    return soma


def media(array_numpy):
    soma = somar(array_numpy)
    qtde_elementos = len(array_numpy)
    media = soma / qtde_elementos

    return media


def variancia(array_numpy):
    _media = media(array_numpy)
    soma = 0
    _variancia = 0

    for el in range(len(array_numpy)):
        soma += math.pow((array_numpy[el] - _media), 2)

    _variancia = soma / len(array_numpy)
    return _variancia


def desvio_padrao(array_numpy):
    return math.sqrt(variancia(array_numpy))


def create_std_deviation_array(array_numpy, deviation_size):
    std_deviation_array = np.empty_like(array_numpy)
    first_element = 0
    last_element = deviation_size
    while True:
        signal_aux = None
        if first_element == 0:
            signal_aux = array_numpy[:last_element]
        else:
            signal_aux = array_numpy[first_element:last_element]

        desvio_padrao_result = desvio_padrao(signal_aux)

        for i in range(first_element, last_element):
            if i < array_numpy.size:
                std_deviation_array[i] = desvio_padrao_result
            else:
                break

        first_element = last_element
        last_element = last_element + deviation_size

        if first_element >= array_numpy.size:
            break

    return normalize_list_numpy(std_deviation_array)


class Audio:
    def __init__(self, audio):
        self.audio = wave.open(audio, 'r')
        self.signal = self.audio.readframes(-1)
        self.signal = np.fromstring(self.signal, np.int16)
        self.fr = self.audio.getframerate()
        self.time = np.linspace(0, len(self.signal) / self.fr, num=(len(self.signal)))
        self.fft = np.fft.fft(self.signal)
        self.signal_normalized = normalize_list_numpy(self.signal)
        self.signal_std_deviation = create_std_deviation_array(self.signal, deviation_size_default)

    def plot(self):
        plt.title("Wave forms")
        plt.plot(self.time, self.signal_std_deviation, '.')


def main(beep_path, sound_path):
    print(beep_path)
    print(sound_path)
    beep = Audio(beep_path)
    mestrado = Audio(sound_path)

    corr = np.correlate(mestrado.signal_std_deviation, beep.signal_std_deviation, mode='same')
    corr_normalized = normalize_list_numpy(corr)

    time_shift = mestrado.time[np.argmax(corr_normalized)]
    print(time_shift)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
