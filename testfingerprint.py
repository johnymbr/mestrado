import decoder as decoder
import fingerprint as fingerprint
import os
import traceback
import sys

filename = "audio.wav"
channels, Fs, file_hash = decoder.read(filename)
result = set()
channel_amount = len(channels)

print("decoder channel_amount %d" % channel_amount)

for channeln, channel in enumerate(channels):
    print("Fingerprint channel %d/%d for %s" % (channeln + 1, channel_amount, filename))
    hashes = fingerprint.fingerprint(channel, Fs=Fs)
    print("Finished channel %d/%d for %s" % (channeln + 1, channel_amount, filename))

    result |= set(hashes)

print("Result: %s", result)
