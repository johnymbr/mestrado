import matplotlib.pyplot as plt
import numpy as np
from statistics import mean
import math as math
import wave
from scipy import signal, fftpack
import sys
from sklearn.preprocessing import normalize, MinMaxScaler, minmax_scale

plt.figure(1)
deviation_size_default = 1000


def normalize_list(list_normal):
    max_value = np.max(list_normal)
    min_value = np.min(list_normal)
    for i in range(len(list_normal)):
        list_normal[i] = (list_normal[i] - min_value) / (max_value - min_value)

    return list_normal


def normalize_list_numpy(list_numpy):
    normalized_list = minmax_scale(list_numpy)
    return normalized_list


def somar(array_numpy):
    soma = 0
    for el in range(len(array_numpy)):
        soma += array_numpy[el]

    return soma


def media(array_numpy):
    soma = somar(array_numpy)
    qtde_elementos = len(array_numpy)
    media = soma / qtde_elementos

    return media


def variancia(array_numpy):
    _media = media(array_numpy)
    soma = 0
    _variancia = 0

    for el in range(len(array_numpy)):
        soma += math.pow((array_numpy[el] - _media), 2)

    _variancia = soma / len(array_numpy)
    return _variancia


def desvio_padrao(array_numpy):
    return math.sqrt(variancia(array_numpy))


def create_std_deviation_array(array_numpy, deviation_size):
    std_deviation_array = np.empty_like(array_numpy)
    first_element = 0
    last_element = deviation_size
    while True:
        signal_aux = None
        if first_element == 0:
            signal_aux = array_numpy[:last_element]
        else:
            signal_aux = array_numpy[first_element:last_element]

        desvio_padrao_result = desvio_padrao(signal_aux)

        for i in range(first_element, last_element):
            if i < array_numpy.size:
                std_deviation_array[i] = desvio_padrao_result
            else:
                break

        first_element = last_element
        last_element = last_element + deviation_size

        if first_element >= array_numpy.size:
            break

    return normalize_list_numpy(std_deviation_array)


class Audio:
    def __init__(self, audio):
        self.audio = wave.open(audio, 'r')
        self.signal = self.audio.readframes(-1)
        self.signal = np.fromstring(self.signal, np.int16)
        self.fr = self.audio.getframerate()
        self.time = np.linspace(0, len(self.signal) / self.fr, num=(len(self.signal)))
        self.fft = np.fft.fft(self.signal)
        self.signal_normalized = normalize_list_numpy(self.signal)
        self.signal_std_deviation = create_std_deviation_array(self.signal, deviation_size_default)
        # self.win = signal.hann(50)
        # self.signal_convolve = signal.convolve(self.signal_std_deviation, self.win, mode='same') / sum(self.win)

    def plot(self):
        plt.title("Wave forms")
        plt.plot(self.time, self.signal_std_deviation, '.')


beep = Audio("beep.wav")
mestrado = Audio("teste_mestrado.wav")

beep.plot()
mestrado.plot()

beep_peaks_cwt = signal.argrelmax(beep.signal)
mestrado_peaks_cwt = signal.argrelmax(mestrado.signal)

# plt.title("Wave forms")
# plt.plot(mestrado.time, mestrado.signal_std_deviation, '.')

# np.savetxt('beep.txt', beep.signal_normalized, fmt='%f')
# np.savetxt('mestrado_full.txt', mestrado.signal, fmt='%f')

# corr = signal.correlate(mestrado.signal_std_deviation, beep.signal_std_deviation, mode='same', method='fft') / sum(
#    mestrado.signal_std_deviation)

corr = np.correlate(mestrado.signal_std_deviation, beep.signal_std_deviation, mode='same')
corr_normalized = normalize_list_numpy(corr)

plt.plot(mestrado.time, corr_normalized, '.')

time_shift = mestrado.time[np.argmax(corr_normalized)]
print(time_shift)
# corr_max = np.max(corr)
# corr_value = corr[int(corr_max)]
# print(corr_value)
plt.show()
print("Finish")
