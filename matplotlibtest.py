#from scipy.io import wavfile
#from matplotlib import pyplot as plt
#import numpy as np

#samplerate, data = wavfile.read('audio.wav')
#times = np.arange(len(data)) / float(samplerate)

#plt.figure(figsize=(30, 4))
#plt.fill_between(times, data)
#plt.xlim(times[0], times[-1])
#plt.xlabel('time (s)')
#plt.ylabel('amplitude')
# You can set the format by changing the extension
# like .pdf, .svg, .eps
#plt.savefig('plot.png', dpi=100)
#plt.show()

import matplotlib.pyplot as plt
from scipy import signal
from scipy.io import wavfile

sample_rate, samples = wavfile.read('audio.wav')
frequencies, times, spectogram = signal.spectrogram(samples, sample_rate)

plt.pcolormesh(times, frequencies, spectogram)
plt.imshow(spectogram)
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [sec]')
plt.show()
