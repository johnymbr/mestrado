from struct import pack
from math import sin, pi
import wave
import random

RATE = 44100

## GENERATE MONO FILE ##
wv = wave.open('test_mono.wav', 'w')
wv.setparams((1, 2, RATE, 0, 'NONE', 'not compressed'))
maxVol = 2 ** 15 - 1.0  # maximum amplitude
wvData = b""
for i in range(0, RATE * 3):
    wvData += pack('h', round(maxVol * sin(i * 2 * pi * 500.0 / RATE)))  # 500Hz
wv.writeframes(wvData)
wv.close()

# generate wav file containing sine waves
# FB36 - 20120617
import math, wave, array

duration = 3  # seconds
freq = 440  # of cycles per second (Hz) (frequency of the sine waves)
volume = 100  # percent
data = array.array('h')  # signed short integer (-32768 to 32767) data
sampleRate = 44100  # of samples per second (standard)
numChan = 1  # of channels (1: mono, 2: stereo)
dataSize = 2  # 2 bytes because of using signed short integers => bit depth = 16
numSamplesPerCyc = int(sampleRate / freq)
numSamples = sampleRate * duration
for i in range(numSamples):
    sample = 32767 * float(volume) / 100
    sample *= math.sin(math.pi * 2 * (i % numSamplesPerCyc) / numSamplesPerCyc)
    data.append(int(sample))
f = wave.open('SineWave_' + str(freq) + 'Hz.wav', 'w')
f.setparams((numChan, dataSize, sampleRate, numSamples, "NONE", "Uncompressed"))
f.writeframes(data.tostring())
f.close()
